import React from "react";

import { createBrowserRouter } from "react-router-dom";

import Layout from "../../components/Layout";
import Board from "../../modules/Board";
import Home from "../../modules/Home";

const routes = createBrowserRouter([
  {
    path: "/:id",
    element: (
      <Layout>
        <Board />
      </Layout>
    ),
  },
  {
    path: "/",
    element: (
      <Layout>
        <Home />
      </Layout>
    ),
  },
]);

export default routes;
