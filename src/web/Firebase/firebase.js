import { getAnalytics } from "firebase/analytics";
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import {
  getStorage,
  ref,
  uploadBytes,
  getDownloadURL,
  deleteObject,
} from "firebase/storage";

// TODO: Replace the following with your app's Firebase project configuration
// See: https://support.google.com/firebase/answer/7015592
const firebaseConfig = {
  apiKey: "AIzaSyDYiflr6y4Hmt_boqxUtbA7j-cD9QXGd7A",
  authDomain: "retro-board-c416e.firebaseapp.com",
  projectId: "retro-board-c416e",
  storageBucket: "retro-board-c416e.appspot.com",
  messagingSenderId: "1098687640851",
  appId: "1:1098687640851:web:16ea68ce1ab34d40c696e6",
  measurementId: "G-NG39NXHBS9",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const analytics = getAnalytics(app);

const storage = getStorage(app);
// const storageRef = ref(storage, 'images');
const storageBox = {
  storage,
  ref,
  uploadBytes,
  getDownloadURL,
  deleteObject,
};

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

export const firebaseData = { db, analytics, storageBox };

export default firebaseData;
