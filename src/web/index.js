import React, { useState } from "react";

import { RouterProvider } from "react-router-dom";

import routes from "./Routes/index";
import { BoardContext } from "../context";

const Index = () => {
  const [boardData, updateBoardData] = useState({});
  return (
    <BoardContext.Provider value={{ boardData, updateBoardData }}>
      <RouterProvider router={routes} />
    </BoardContext.Provider>
  );
};

export default Index;
