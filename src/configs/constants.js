export const DEFAULT_COLUMNS = [
  {
    id: "went_well",
    label: "Went Well",
    order: 0,
    color: "#009782",
  },
  {
    id: "to_improve",
    label: "To Improve",
    order: 1,
    color: "#e62865",
  },
  {
    id: "action_items",
    label: "Action Items",
    order: 2,
    color: "#747775",
  },
];
