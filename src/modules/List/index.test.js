import React from "react";

import { render, screen } from "@testing-library/react";

import useListData from "./hooks/useListData";
import List from "./index";

function MockCard({ card }) {
  return <div data-testid="card">{card.content}</div>;
}

function MockAddCard() {
  return <div data-testid="add-card">Add card</div>;
}

jest.mock("../Card/Card", () => MockCard);
jest.mock("../Card/AddCard", () => MockAddCard);

// Mocking useListData hook
jest.mock("./hooks/useListData", () => jest.fn());

describe("List component", () => {
  test("renders list with column label and cards", () => {
    // Mock data
    const column = { id: 1, label: "Column 1" };
    useListData.mockReturnValueOnce({
      listData: [
        { id: 1, content: "Card 1" },
        { id: 2, content: "Card 2" },
      ],
    });

    // Render the component
    const { getByText, getByTestId } = render(<List column={column} />);

    // Assert that the column label is rendered
    expect(getByText("Column 1")).toBeInTheDocument();

    // Assert that AddCard component is rendered
    expect(getByTestId("add-card")).toBeInTheDocument();

    // Assert that Card components are rendered for each card in listData
    expect(getByText("Card 1")).toBeInTheDocument();
    expect(getByText("Card 2")).toBeInTheDocument();
  });

  test("renders list with column label and no cards", () => {
    // Mock data
    const column = { id: 1, label: "Column 1" };
    useListData.mockReturnValueOnce({
      listData: [],
    });

    // Render the component
    const { getByText, getByTestId } = render(<List column={column} />);

    // Assert that the column label is rendered
    expect(getByText("Column 1")).toBeInTheDocument();

    // Assert that AddCard component is rendered
    expect(getByTestId("add-card")).toBeInTheDocument();

    // Assert that Card components are rendered for each card in listData
    expect(screen.queryByTestId("card")).toBeFalsy();
  });
});
