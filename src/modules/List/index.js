import React from "react";

import AddCard from "./../Card/AddCard";
import Card from "./../Card/Card";
import useListData from "./hooks/useListData";
import { ListContext } from "../../context";

const List = ({ column }) => {
  const { listData, ...rest } = useListData({ column });
  return (
    <ListContext.Provider value={{ column, listData, ...rest }}>
      <div className="bg-white m-4 rounded h-fit">
        <h3 className="p-4 font-bold">{column.label}</h3>
        <div className="m-2">
          <AddCard />
        </div>
        <div>
          {listData.map((card) => (
            <Card card={card} key={card.id} />
          ))}
        </div>
      </div>
    </ListContext.Provider>
  );
};

export default List;
