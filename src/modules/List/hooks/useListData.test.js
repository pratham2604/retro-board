import React from "react";

import { renderHook } from "@testing-library/react";
import { act } from "react-dom/test-utils";

import useListData from "./useListData";
import { BoardContext } from "../../../context";

describe("useListData hook", () => {
  beforeEach(() => {
    // Clear any previous mock calls before each test
    jest.clearAllMocks();
  });

  test("returns correct listData when cards exist", () => {
    // Mock data
    const column = { id: 1 };
    const cards = [
      { id: 1, columnId: 1, updatedOn: 2 },
      { id: 2, columnId: 1, updatedOn: 1 },
    ];

    // Render the hook
    const { result } = renderHook(() => useListData({ column }), {
      wrapper: ({ children }) => (
        <BoardContext.Provider value={{ boardData: { cards } }}>
          {children}
        </BoardContext.Provider>
      ),
    });

    // Assert that listData contains the correct cards and is sorted by updatedOn
    expect(result.current.listData).toEqual([cards[0], cards[1]]);
  });

  test("returns correct listData when cardData is set", () => {
    // Mock data
    const column = { id: 1 };
    const cards = [
      { id: 1, columnId: 1, updatedOn: 2 },
      { id: 2, columnId: 1, updatedOn: 1 },
    ];

    // Render the hook
    const { result, rerender } = renderHook(() => useListData({ column }), {
      wrapper: ({ children }) => (
        <BoardContext.Provider value={{ boardData: { cards } }}>
          {children}
        </BoardContext.Provider>
      ),
    });

    // Set cardData
    act(() => {
      result.current.setCardData({ id: 1 });
    });

    // Rerender the hook
    rerender();

    // Assert that listData contains the correct cards and is sorted by updatedOn
    expect(result.current.listData).toEqual([cards[1]]);
  });
});
