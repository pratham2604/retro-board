import { useContext, useState } from "react";

import { BoardContext } from "../../../context";

const useListData = ({ column }) => {
  const [isEditMode, setEditMode] = useState(false);
  const { boardData: { cards = [] } = {} } = useContext(BoardContext);
  const [cardData, setCardData] = useState({});

  const listData =
    cards
      .filter((card) => card.columnId === column.id && cardData.id !== card.id)
      .sort((a, b) => b.updatedOn - a.updatedOn) || [];

  return {
    listData,
    isEditMode,
    setEditMode,
    cardData,
    setCardData,
  };
};

export default useListData;
