import { useState, useEffect, useContext, useCallback } from "react";

import { v4 as uuid } from "uuid";

import CardAPIs from "../../../apis/Card";
import { BoardContext, ListContext } from "../../../context";

const useCardFormHandler = () => {
  const { boardData, updateBoardData } = useContext(BoardContext);
  const { setEditMode, cardData, column, setCardData } =
    useContext(ListContext);

  const [value, setValue] = useState(cardData.content || "");
  const [saving, setSaving] = useState(false);

  const onInputChange = useCallback((value) => {
    setValue(value);
  }, []);

  const onSubmit = useCallback(async () => {
    setSaving(true);
    let data = {};
    const newCards = boardData.cards;
    if (cardData.id) {
      data = { ...cardData, content: value, updatedOn: new Date().valueOf() };
      const targetIndex = newCards.findIndex((card) => card.id === cardData.id);
      newCards[targetIndex] = data;
    } else {
      data = {
        id: uuid(),
        content: value,
        likes: 0,
        boardId: boardData.id,
        columnId: column.id,
        updatedOn: new Date().valueOf(),
      };
      newCards.push(data);
    }

    // update database
    await CardAPIs.createCard(data);
    setEditMode(false);
    setSaving(false);

    // local update
    updateBoardData(() => ({
      ...boardData,
      cards: newCards,
    }));
    setCardData({});
  }, [
    cardData,
    column.id,
    setEditMode,
    value,
    boardData,
    setCardData,
    updateBoardData,
  ]);

  const onCancelEdit = () => {
    setEditMode(false);
    setCardData({});
  };

  const setInputValue = useCallback(() => {
    setValue(cardData.content || "");
  }, [cardData.content]);

  useEffect(() => {
    setInputValue();
  }, [setInputValue]);

  return {
    cardData,
    value,
    onInputChange,
    onSubmit,
    saving,
    onCancelEdit,
  };
};

export default useCardFormHandler;
