import { useState, useCallback, useContext } from "react";

import CardAPIs from "../../../apis/Card";
import { BoardContext, ListContext } from "../../../context";

const useAddCardData = ({ card }) => {
  const [showOptions, setShowOptions] = useState(false);
  const [onDeleting, setDeleting] = useState(false);
  const { boardData, updateBoardData } = useContext(BoardContext);
  const { isEditMode, setEditMode, setCardData, cardData, column } =
    useContext(ListContext);

  const toggleEditMode = useCallback(() => {
    setEditMode((value) => !value);
  }, [setEditMode]);

  const toogleShowOptions = useCallback(() => {
    setShowOptions((value) => !value);
  }, []);

  const onEdit = useCallback(() => {
    setShowOptions(false);
    setEditMode(true);
    setCardData(card);
  }, [setEditMode, setShowOptions, setCardData, card]);

  const onDelete = useCallback(async () => {
    setDeleting(true);
    await CardAPIs.deleteCard(card.id);
    updateBoardData(() => ({
      ...boardData,
      cards: boardData.cards.filter(
        (currentCard) => currentCard.id !== card.id,
      ),
    }));
    setDeleting(false);
  }, [boardData, card?.id, updateBoardData]);

  return {
    isEditMode,
    toggleEditMode,
    boardData,
    setShowOptions,
    toogleShowOptions,
    showOptions,
    onEdit,
    onDelete,
    cardData,
    color: column.color,
    onDeleting,
  };
};

export default useAddCardData;
