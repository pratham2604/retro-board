import React from "react";

import { render, fireEvent } from "@testing-library/react";

import AddCardForm from "./AddCardForm";
import useCardFormHandler from "./hooks/useCardFormHandler";

// Mocking useCardFormHandler hook
jest.mock("./hooks/useCardFormHandler", () => jest.fn());

describe("AddCardForm component", () => {
  beforeEach(() => {
    // Clear any previous mock calls before each test
    jest.clearAllMocks();
  });

  test("renders input field and submit/cancel buttons", () => {
    // Mock data
    useCardFormHandler.mockReturnValueOnce({
      value: "",
      onInputChange: jest.fn(),
      onSubmit: jest.fn(),
      saving: false,
      onCancelEdit: jest.fn(),
    });

    // Render the component
    const { getByPlaceholderText, getByText } = render(<AddCardForm />);

    // Assert that the input field is rendered
    expect(getByPlaceholderText("Enter card title")).toBeInTheDocument();

    // Assert that the submit button is rendered
    expect(getByText("Submit")).toBeInTheDocument();

    // Assert that the cancel button is rendered
    expect(getByText("Cancel")).toBeInTheDocument();
  });

  test("calls onInputChange when input value changes", () => {
    // Mock onInputChange function
    const mockOnInputChange = jest.fn();
    useCardFormHandler.mockReturnValueOnce({
      value: "",
      onInputChange: mockOnInputChange,
      onSubmit: jest.fn(),
      saving: false,
      onCancelEdit: jest.fn(),
    });

    // Render the component
    const { getByPlaceholderText } = render(<AddCardForm />);

    // Change the input value
    fireEvent.change(getByPlaceholderText("Enter card title"), {
      target: { value: "New title" },
    });

    // Assert that onInputChange is called with the correct value
    expect(mockOnInputChange).toHaveBeenCalledWith("New title");
  });

  test("calls onSubmit when submit button is clicked and value is not empty", () => {
    // Mock onSubmit function
    const mockOnSubmit = jest.fn();
    useCardFormHandler.mockReturnValueOnce({
      value: "Test",
      onInputChange: jest.fn(),
      onSubmit: mockOnSubmit,
      saving: false,
      onCancelEdit: jest.fn(),
    });

    // Render the component
    const { getByText } = render(<AddCardForm />);

    const element = getByText("Submit");

    // Click the submit button
    fireEvent.click(element);

    // Assert that onSubmit is called
    expect(mockOnSubmit).toHaveBeenCalled();
  });

  test("submit button is disabled when value is empty", () => {
    // Mock onSubmit function
    const mockOnSubmit = jest.fn();
    useCardFormHandler.mockReturnValueOnce({
      value: "",
      onInputChange: jest.fn(),
      onSubmit: mockOnSubmit,
      saving: false,
      onCancelEdit: jest.fn(),
    });

    // Render the component
    const { getByText } = render(<AddCardForm />);

    const element = getByText("Submit");

    expect(element).toBeDisabled();
  });

  test("calls onCancelEdit when cancel button is clicked", () => {
    // Mock onCancelEdit function
    const mockOnCancelEdit = jest.fn();
    useCardFormHandler.mockReturnValueOnce({
      value: "",
      onInputChange: jest.fn(),
      onSubmit: jest.fn(),
      saving: false,
      onCancelEdit: mockOnCancelEdit,
    });

    // Render the component
    const { getByText } = render(<AddCardForm />);

    // Click the cancel button
    fireEvent.click(getByText("Cancel"));

    // Assert that onCancelEdit is called
    expect(mockOnCancelEdit).toHaveBeenCalled();
  });
});
