import React from "react";

import AddCardForm from "./AddCardForm";
import useAddCardData from "./hooks/useAddCardData";
import Button from "../../components/Button";

const AddCard = () => {
  const { isEditMode, toggleEditMode } = useAddCardData({});
  return (
    <div>
      {isEditMode ? (
        <AddCardForm />
      ) : (
        <Button
          className="bg-slate-500 hover:bg-slate-700 disabled:bg-slate-400 w-full"
          onClick={toggleEditMode}
        >
          <span className="icon-plus"></span>
          <span className="font-bold"> Add card</span>
        </Button>
      )}
    </div>
  );
};

export default AddCard;
