import React from "react";

import { Popover } from "react-tiny-popover";

import useAddCardData from "./hooks/useAddCardData";

const Card = ({ card }) => {
  const {
    showOptions,
    setShowOptions,
    toogleShowOptions,
    onEdit,
    color,
    onDelete,
    onDeleting,
  } = useAddCardData({
    card,
  });
  return (
    <div className="p-4 m-2 flex rounded" style={{ backgroundColor: color }}>
      <div className="flex-1">{card.content}</div>
      <div>
        <button className="icon-heart-o p-2" onClick={toogleShowOptions} />
      </div>
      <div>
        <Popover
          isOpen={showOptions}
          reposition={true}
          positions={["bottom"]}
          onClickOutside={() => setShowOptions(false)}
          content={
            <CardOptions
              onEdit={onEdit}
              onDelete={onDelete}
              onDeleting={onDeleting}
            />
          }
        >
          <button className="icon-ellipsis-v p-2" onClick={toogleShowOptions} />
        </Popover>
      </div>
    </div>
  );
};

const CardOptions = ({ onEdit, onDelete, onDeleting }) => (
  <div className="bg-white rounded border-2 w-24">
    <div className="p-2 border-b-2">
      <button
        onClick={onEdit}
        className="text-center w-full disabled:cursor-not-allowed disabled:text-slate-400"
        disabled={onDeleting}
      >
        Edit
      </button>
    </div>
    <div className="p-2">
      <button
        onClick={onDelete}
        disabled={onDeleting}
        className="text-center w-full text-red-500 disabled:cursor-not-allowed disabled:text-slate-400"
      >
        Delete
      </button>
    </div>
  </div>
);

export default Card;
