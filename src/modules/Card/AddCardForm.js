import React from "react";

import useCardFormHandler from "./hooks/useCardFormHandler";
import Button from "../../components/Button";

const AddCardForm = () => {
  const { value, onInputChange, onSubmit, saving, onCancelEdit } =
    useCardFormHandler();

  return (
    <div className="round bg-slate-400 p-4 rounded">
      <input
        className="p-2 rounded w-full mb-2"
        value={value}
        onChange={(e) => onInputChange(e.target.value)}
        placeholder="Enter card title"
      />
      <div className="text-right">
        <Button
          className="bg-blue-500 hover:bg-blue-700 disabled:bg-blue-400 disabled:cursor-not-allowed"
          disabled={!value || saving}
          onClick={onSubmit}
        >
          Submit
        </Button>
        <Button
          className="bg-red-500 hover:bg-red-700 disabled:bg-red-400 ml-2 disabled:cursor-not-allowed"
          onClick={onCancelEdit}
          disabled={saving}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default AddCardForm;
