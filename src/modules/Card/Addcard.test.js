import React from "react";

import { render, fireEvent } from "@testing-library/react";

import AddCard from "./AddCard";
import useAddCardData from "./hooks/useAddCardData";

function MockAddCardForm() {
  return <div data-testid="add-card-form">Mock AddCard Form</div>;
}

jest.mock("./AddCardForm", () => MockAddCardForm);

// Mocking useAddCardData hook
jest.mock("./hooks/useAddCardData", () => jest.fn());

describe("AddCard component", () => {
  beforeEach(() => {
    // Clear any previous mock calls before each test
    jest.clearAllMocks();
  });

  test("renders add card button when not in edit mode", () => {
    // Mock isEditMode as false
    const mockUseAddCardData = jest.fn(() => ({
      isEditMode: false,
      toggleEditMode: jest.fn(),
    }));
    useAddCardData.mockReturnValueOnce({
      isEditMode: false,
    });
    require("./hooks/useAddCardData").default = mockUseAddCardData;

    // Render the component
    const { getByText } = render(<AddCard />);

    // Assert that the add card button is rendered
    expect(getByText("Add card")).toBeInTheDocument();
  });

  test("calls toggleEditMode when add card button is clicked", () => {
    // Mock toggleEditMode function
    const mockToggleEditMode = jest.fn();
    useAddCardData.mockReturnValueOnce({
      isEditMode: false,
      toggleEditMode: mockToggleEditMode,
    });

    // Render the component
    const { getByText } = render(<AddCard />);

    // Click the add card button
    fireEvent.click(getByText("Add card"));

    // Assert that toggleEditMode is called
    expect(mockToggleEditMode).toHaveBeenCalled();
  });

  test("renders AddCardForm when in edit mode", () => {
    // Mock isEditMode as true
    const mockToggleEditMode = jest.fn();
    useAddCardData.mockReturnValueOnce({
      isEditMode: true,
      toggleEditMode: mockToggleEditMode,
    });

    // Render the component
    const { getByTestId } = render(<AddCard />);

    // Assert that AddCardForm is rendered
    expect(getByTestId("add-card-form")).toBeInTheDocument();
  });
});
