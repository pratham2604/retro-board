import React from "react";

import useDataHandler from "./hooks/useDataHandler";
import Button from "../../components/Button";

const Home = () => {
  const { createNewBoard, creatingBoard } = useDataHandler() || {};
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 m-4">
      <div className="justify-center items-center flex m-4 p-4 bg-slate-400 rounded">
        <Button
          className="bg-blue-500 hover:bg-blue-700 disabled:bg-blue-400 font-bold"
          onClick={createNewBoard}
          loading={creatingBoard}
        >
          Create New Board
        </Button>
      </div>
    </div>
  );
};

export default Home;
