import React from "react";

import { render, fireEvent } from "@testing-library/react";

import useDataHandler from "./hooks/useDataHandler";
import Home from "./index";

// Mocking useDataHandler hook
jest.mock("./hooks/useDataHandler", () => jest.fn());

describe("Home component", () => {
  beforeEach(() => {
    // Clear any previous mock calls before each test
    jest.clearAllMocks();
  });

  test("renders create new board button", () => {
    useDataHandler.mockReturnValueOnce({
      createNewBoard: jest.fn(),
      creatingBoard: false,
    });

    const { getByText } = render(<Home />);
    const createButton = getByText("Create New Board");
    expect(createButton).toBeInTheDocument();
  });

  test("calls createNewBoard function when button is clicked", () => {
    const mockCreateNewBoard = jest.fn();
    useDataHandler.mockReturnValueOnce({
      createNewBoard: mockCreateNewBoard,
      creatingBoard: false,
    });

    const { getByText } = render(<Home />);
    const createButton = getByText("Create New Board");
    fireEvent.click(createButton);
    expect(mockCreateNewBoard).toHaveBeenCalled();
  });

  test("disables button while creatingBoard is true", () => {
    useDataHandler.mockReturnValueOnce({
      createNewBoard: jest.fn(),
      creatingBoard: true,
    });

    const { getByText } = render(<Home />);
    const createButton = getByText("Create New Board");
    expect(createButton).toBeDisabled();
  });
});
