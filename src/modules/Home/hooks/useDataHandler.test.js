import { renderHook, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";

import useDataHandler from "./useDataHandler";

// Mocking dependencies
jest.mock("react-router-dom", () => ({
  useNavigate: jest.fn(),
}));
jest.mock("../../../apis/Board", () => ({
  createBoard: jest.fn(),
}));

describe("useDataHandler custom hook", () => {
  beforeEach(() => {
    // Clear any previous mock calls before each test
    jest.clearAllMocks();
  });

  test("Creating new board", async () => {
    const mockNavigate = jest.fn();
    const mockCreateBoard = jest.fn().mockResolvedValueOnce({ id: "mockId" });

    // Mock useNavigate hook to return mockNavigate function
    require("react-router-dom").useNavigate.mockReturnValue(mockNavigate);
    // Mock createBoard API function to return a resolved promise
    require("../../../apis/Board").createBoard = mockCreateBoard;

    // Render the hook
    const { result } = renderHook(() => useDataHandler());

    // Call createNewBoard function
    act(() => {
      result.current.createNewBoard();
    });

    // Assert that creatingBoard is set to true after calling createNewBoard
    expect(result.current.creatingBoard).toBe(true);

    // Wait for the next update, which indicates that createNewBoard has finished
    await waitFor(() => {
      // Assert that createBoard API function is called with a new UUID
      expect(mockCreateBoard).toHaveBeenCalledWith(expect.any(String));

      // Assert that useNavigate is called with the correct path
      expect(mockNavigate).toHaveBeenCalledWith("/mockId");

      // Assert that creatingBoard is set back to false after API call
      expect(result.current.creatingBoard).toBe(false);
    });
  });
});
