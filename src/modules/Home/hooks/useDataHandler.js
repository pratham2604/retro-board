import { useCallback, useState } from "react";

import { useNavigate } from "react-router-dom";
import { v4 as uuid } from "uuid";

import BoardAPIs from "../../../apis/Board";

const useDataHandler = () => {
  const navigate = useNavigate();
  const [creatingBoard, setCreatingBoard] = useState(false);

  const createNewBoard = useCallback(async () => {
    const newId = uuid();
    setCreatingBoard(true);
    const response = await BoardAPIs.createBoard(newId);
    navigate(`/${response.id}`);
    setCreatingBoard(false);
  }, [navigate]);

  return {
    createNewBoard,
    creatingBoard,
  };
};

export default useDataHandler;
