import React from "react";

import { render, screen } from "@testing-library/react";

import useDataHandler from "./hooks/useDataHandler";
import Board from "./index";

// Mocking useDataHandler hook
jest.mock("./hooks/useDataHandler", () => jest.fn());

function MockList({ column }) {
  return <div data-testid="list">{column.label}</div>;
}

jest.mock("../List/index", () => MockList);

describe("Board component", () => {
  test("renders EmptyData when no data is fetching and columns are empty", () => {
    // Mocking useDataHandler to return no fetching data and empty columns
    useDataHandler.mockReturnValueOnce({ fetchingData: false, columns: [] });

    const { getByTestId } = render(<Board />);
    const emptyDataElement = getByTestId("empty-data");

    expect(emptyDataElement).toBeInTheDocument();
  });

  test("renders List components when data is available", () => {
    // Mocking useDataHandler to return non-empty columns
    useDataHandler.mockReturnValueOnce({
      fetchingData: false,
      columns: [
        { id: "went_well", label: "Went Well" },
        { id: "to_improve", label: "To Improve" },
      ],
    });

    const { getAllByTestId, getByText } = render(<Board />);
    const listElements = getAllByTestId("list");
    expect(listElements[0]).toBeInTheDocument();

    expect(listElements).toHaveLength(2);

    const wentWellElement = getByText("Went Well");
    expect(wentWellElement).toBeInTheDocument();
  });

  test("renders nothing when data is fetching", () => {
    // Mocking useDataHandler to return fetching data
    useDataHandler.mockReturnValueOnce({ fetchingData: true, columns: [] });

    render(<Board />);
    const listElement = screen.queryByTestId("list");
    expect(listElement).toBeFalsy();

    const emptyDataElement = screen.queryByTestId("empty-data");
    expect(emptyDataElement).toBeFalsy();
  });
});
