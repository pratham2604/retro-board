import { useEffect, useState, useContext } from "react";

import BoardAPIs from "../../../apis/Board";
import { BoardContext } from "../../../context";

const useDataHandler = () => {
  const { updateBoardData } = useContext(BoardContext);
  const [fetchingData, setFetchingData] = useState(false);
  const [data, setData] = useState({});
  const boardId = window.location.pathname.split("/").pop();

  useEffect(() => {
    setFetchingData(true);
    BoardAPIs.fetchData(boardId)
      .then((data) => {
        setData(data);
        updateBoardData(data);
        setFetchingData(false);
        return BoardAPIs.fetchListData(boardId);
      })
      .then((data) => {
        updateBoardData((value) => ({
          ...value,
          cards: data,
        }));
        setFetchingData(false);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
        setFetchingData(false);
      });
  }, [boardId, updateBoardData]);

  return {
    columns: data?.columns || [],
    fetchingData,
    boardId: data?.id || "",
  };
};

export default useDataHandler;
