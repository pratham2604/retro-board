// useDataHandler.test.js
import React from "react";

import { renderHook, waitFor } from "@testing-library/react";

import BoardAPIs from "./../../../apis/Board";
import useDataHandler from "./useDataHandler";
import { BoardContext } from "../../../context";

// Mocking the fetchData and fetchListData functions
jest.mock("./../../../apis/Board", () => ({
  fetchData: jest.fn(),
  fetchListData: jest.fn(),
}));

const mockedData = { id: "123", columns: [], title: "test" };

describe("useDataHandler custom hook", () => {
  test("should fetch board data and return fetchingData, columns and boardId", async () => {
    // Mock fetchData to resolve with mocked data
    BoardAPIs.fetchData.mockResolvedValue(mockedData);
    const mockUpdateBoardData = jest.fn();

    // Render the hook within a context provider
    const { result } = renderHook(() => useDataHandler(), {
      wrapper: ({ children }) => (
        <BoardContext.Provider value={{ updateBoardData: mockUpdateBoardData }}>
          {children}
        </BoardContext.Provider>
      ),
    });

    // Ensure fetchingData is true initially
    expect(result.current.fetchingData).toBe(true);

    await waitFor(() => {
      // Assert that the hook returns the correct data
      expect(result.current.fetchingData).toBe(false);
      expect(result.current.columns).toEqual([]);
      expect(result.current.boardId).toBe("123");
    });
  });
});
