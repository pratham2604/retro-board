import React from "react";

import List from "./../List";
import useDataHandler from "./hooks/useDataHandler";
import EmptyData from "../../components/EmptyData";

const Board = () => {
  const { fetchingData, columns } = useDataHandler();

  if (!fetchingData && columns.length === 0) {
    return <EmptyData />;
  }

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 m-4">
      {columns.map((column) => (
        <List column={column} key={column.id} />
      ))}
    </div>
  );
};

export default Board;
