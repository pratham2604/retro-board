import React from "react";

import { render, screen } from "@testing-library/react";

import EmotyData from "./index";

test("Renders empty data block", () => {
  render(<EmotyData />);
  const placeholderElement = screen.getByText("No data found");
  expect(placeholderElement).toBeInTheDocument();
});
