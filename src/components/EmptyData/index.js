import React from "react";

const EmptyData = () => (
  <div
    data-testid="empty-data"
    className="m-4 p-8 text-center text-3xl bg-white"
  >
    No data found
  </div>
);

export default EmptyData;
