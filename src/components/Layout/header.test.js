import React from "react";

import { render } from "@testing-library/react";

import Header from "./header";

describe("Header component", () => {
  test("renders without crashing", () => {
    render(<Header />);
  });

  test("renders with correct title", () => {
    const { getByText } = render(<Header />);
    const titleElement = getByText("Retro Board");
    expect(titleElement).toBeInTheDocument();
  });

  test("renders with correct styles", () => {
    const { container } = render(<Header />);
    const headerDiv = container.firstChild;
    expect(headerDiv).toHaveClass("shadow-md");
    expect(headerDiv).toHaveClass("w-full");
    expect(headerDiv).toHaveClass("fixed");
    expect(headerDiv).toHaveClass("top-0");
    expect(headerDiv).toHaveClass("left-0");

    const titleElement = headerDiv.querySelector(".font-bold");
    expect(titleElement).toHaveTextContent("Retro Board");
    expect(titleElement).toHaveClass("text-2xl");
  });
});
