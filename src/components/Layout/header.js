import React from "react";

const Header = () => (
  <div className="shadow-md w-full fixed top-0 left-0">
    <div className="md:flex bg-white p-4">
      <div className="font-bold text-2xl">Retro Board</div>
    </div>
  </div>
);

export default Header;
