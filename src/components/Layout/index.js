import React from "react";

import Header from "./header";

const Layout = ({ children }) => (
  <div>
    <Header />
    <div className="bg-indigo-100 w-full h-screen overflow-y-auto pt-16">
      {children}
    </div>
  </div>
);

export default Layout;
