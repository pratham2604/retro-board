import React from "react";

import { render, fireEvent } from "@testing-library/react";

import Button from "./index";

describe("Button component", () => {
  test("renders button with text", () => {
    const { getByText } = render(<Button>Click me</Button>);
    const button = getByText("Click me");
    expect(button).toBeInTheDocument();
  });

  test("button onClick function is called when clicked", () => {
    const onClickMock = jest.fn();
    const { getByText } = render(
      <Button onClick={onClickMock}>Click me</Button>,
    );
    const button = getByText("Click me");
    fireEvent.click(button);
    expect(onClickMock).toHaveBeenCalledTimes(1);
  });

  test("button is disabled when disabled prop is true", () => {
    const { getByText } = render(
      <Button disabled loading={true}>
        Click me
      </Button>,
    );
    const button = getByText("Click me");
    expect(button).toBeDisabled();
  });

  test("button is not disabled when disabled prop is false", () => {
    const { getByText } = render(<Button disabled={false}>Click me</Button>);
    const button = getByText("Click me");
    expect(button).toBeEnabled();
  });

  test("button shows loading state when loading prop is true", () => {
    const { getByTestId } = render(<Button loading>Click me</Button>);
    const loadingIndicator = getByTestId("loading-indicator");
    expect(loadingIndicator).toBeInTheDocument();
  });

  test("button does not show loading state when loading prop is false", () => {
    const { queryByTestId } = render(<Button loading={false}>Click me</Button>);
    const loadingIndicator = queryByTestId("loading-indicator");
    expect(loadingIndicator).toBeNull();
  });
});
