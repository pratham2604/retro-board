import React from "react";

import Loader from "../Loader";

const Button = ({ children, onClick, className, disabled, loading }) => (
  <button
    className={`${className} py-2 px-4 rounded text-white`}
    onClick={onClick}
    disabled={disabled || loading}
  >
    {loading && (
      <Loader className="inline-block postion-relative top-4 mr-4 w-4 h-4" />
    )}
    {children}
  </button>
);

export default Button;
