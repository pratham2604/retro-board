import React from "react";

const Loader = ({ style, className }) => (
  <div
    data-testid="loading-indicator"
    className={`loader ${className}`}
    style={style}
  ></div>
);

export default Loader;
