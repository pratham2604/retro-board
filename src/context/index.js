import { createContext } from "react";

export const BoardContext = createContext({});

export const ListContext = createContext({});
