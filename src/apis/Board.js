import {
  doc,
  setDoc,
  getDoc,
  query,
  collection,
  where,
  getDocs,
} from "firebase/firestore";

import { DEFAULT_COLUMNS } from "./../configs/constants";
import firebaseData from "./../web/Firebase/firebase";

const BoardAPIs = {
  createBoard: async (newBoardId) => {
    const data = {
      id: newBoardId,
      title: `New Board ${newBoardId}`,
      columns: DEFAULT_COLUMNS,
    };
    await setDoc(doc(firebaseData.db, "boards", newBoardId), data);
    return data;
  },

  fetchData: async (boardId) => {
    const docSnap = await getDoc(doc(firebaseData.db, "boards", boardId));
    let document = null;
    if (docSnap.exists()) {
      document = { ...docSnap.data() };
    }
    return document;
  },

  fetchListData: async (boardId) => {
    const docSnap = await query(
      collection(firebaseData.db, "cards"),
      where("boardId", "==", boardId),
    );
    const querySnapshot = await getDocs(docSnap);
    const documents = [];
    querySnapshot.forEach((doc) => {
      documents.push(doc.data());
    });
    return documents;
  },
};

export default BoardAPIs;
