import { doc, setDoc, deleteDoc } from "firebase/firestore";

import firebaseData from "./../web/Firebase/firebase";

const CardAPIs = {
  createCard: async (data) => {
    await setDoc(doc(firebaseData.db, "cards", data.id), data);
    return data;
  },

  deleteCard: async (cardId) => {
    await deleteDoc(doc(firebaseData.db, "cards", cardId));
  },
};

export default CardAPIs;
