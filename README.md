# Retro Board



## Getting started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)(#editing-this-readme)!

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Running project

### `npm install`

Installs dependecies of project required to run on local
### `npm start`

Runs the app in the development mode.\

## Live demp

Open [Demo Retro Board](https://retro-board-c416e.web.app) to see working demo.