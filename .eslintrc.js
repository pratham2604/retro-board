module.exports = {
  extends: [
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:react/recommended",
    "plugin:jsx-a11y/recommended",
  ],
  plugins: ["react", "import", "jsx-a11y", "react-hooks", "prettier"],
  rules: {
    "no-unused-vars": [
      "error",
      {
        varsIgnorePattern: "React",
      },
    ],
    "react/prop-types": 0,
    "prettier/prettier": 1,
    indent: ["error", 2],
    "linebreak-style": 1,
    quotes: ["error", "double"],
    "max-params": ["error", 3],
    "no-console": ["error", { allow: ["warn", "error"] }],
    "arrow-body-style": ["error", "as-needed"],
    "import/order": [
      "error",
      {
        groups: ["builtin", "external", "internal"],
        pathGroups: [
          {
            pattern: "react",
            group: "external",
            position: "before",
          },
          {
            pattern: "~/**",
            group: "internal",
            position: "after",
          },
          {
            pattern: "@mindtickle/**",
            group: "external",
            position: "after",
          },
          {
            pattern: "mt-*/**",
            group: "external",
            position: "after",
          },
        ],
        pathGroupsExcludedImportTypes: ["react"],
        "newlines-between": "always",
        alphabetize: {
          order: "asc",
          caseInsensitive: true,
        },
      },
    ],
  },
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    es2021: true,
    browser: true,
    node: true,
    jest: true,
  },
  settings: {
    react: {
      version: "detect",
    },
  },
};
